#!/bin/sh

PATH=/usr/bin
TMUX=/usr/bin/tmux

echo -n "checking to see that ${TMUX} is an ELF executable..."

case "$(file -b --mime-type "${TMUX}")" in
    application/x-executable|application/x-pie-executable|application/x-sharedlib)
        echo "ok"
        RET=0
        ;;
    *)
        echo "FAIL"
        RET=1
        ;;
esac

exit ${RET}
