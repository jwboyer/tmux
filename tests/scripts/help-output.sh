#!/bin/sh

PATH=/usr/bin
TMUX=/usr/bin/tmux

echo -n "checking for ${TMUX} --help output and exit code..."

STDOUT="$(${TMUX} --help 2>/dev/null)"
STDERR="$(${TMUX} --help 2>&1 | head -n 1 | cut -c-11)"
${TMUX} --help >/dev/null 2>&1
EXITCODE=$?

if [ ${EXITCODE} -eq 1 ] && [ -z "${STDOUT}" ] && [ "${STDERR}" = "usage: tmux" ]; then
    echo "ok"
    RET=0
else
    echo "FAIL"
    RET=1
fi

exit ${RET}
